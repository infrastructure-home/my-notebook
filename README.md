Ansible script for setting up my notebooks

# Usage

```bash
ansible-playbook --ask-vault-pass --ask-become-pass -i production -l <device> site.yml
```

Note: for production I use ask-become-pass, because I don't want to store my admin password in the git repo, even not in the vault files.

# Testing
## Virtual Machine
If it comes to testing distribution specific stuff, you need VirtualBox with a fresh instance of your desired OS installed.

```bash
ansible-playbook --ask-vault-pass -i staging site.yml
```


# How to set up a new device
- Install OS with the admin account
- Manual install a ssh server with defaults (password authentication on)
    ```bash
    sudo apt install openssh-server
    ```
- Create a unique ssh key pair for this device
- exchange key files over ```ssh-copy-id -i <key_file> <admin-user>@<ip>```
- Set up a host var file for the device and also add it to the desired production groups
- Execute playbook for production limited to the device
    ```bash
    ansible-playbook --ask-vault-pass --ask-become-pass -i production -l <device> site.yml
    ```

Note: there is no need to manual take care of the ssh hardening, as this is covered by an ansible task
